﻿using UnityEngine;
using System.Collections;

public class Sgn_sale : MonoBehaviour {

	public int				nRank;
	public int				nRankMax;
	public static float		fDiscount;
	public float[]			fPositionX;
	public string			InputValueText;
	public string			ValueText;
	public GameObject		NumberPrefab;

	// Use this for initialization
	void Start () {

		Sgn_sale_Init ();
		CreateNumberObject ();

	}
	
	// Update is called once per frame
	void Update () {
	
		SetActiveNumber ();

	}

	public void		Sgn_sale_Init(){

		nRank 			= 1;
		nRankMax 		= 2;
		fDiscount 		= 0.0f;
		fPositionX 		= new float[]{ 38.0f, 17.0f };
		InputValueText 	= "";
		ValueText 		= "0";

	}

	public void		Sgn_sale_Reset(){
		
		for( int i = 0 ; i < nRank ; i++ ){
			
			transform.GetChild (i).gameObject.SetActive ( false );
			transform.GetChild (i).GetComponent<UISprite>().spriteName 	= "num_s_00";
			transform.GetChild (i).GetComponent<NumberNode>().nNumber 	= 0;
			
		}
		nRank 			= 1;
		fDiscount		= 0.0f;
		ValueText 		= "0";
		
	}

	public void		CreateNumberObject(){

		for( int i = 0 ; i < nRankMax ; i++ ) CreateNumber( "num_s_00", 0, i, fPositionX[i] );

	}

	public void		CreateNumber( string Name, int nNum, int nID , float fPositionX){

		GameObject Obj = Instantiate (NumberPrefab) as GameObject;
		Obj.transform.parent = transform;

		NumberNode		NumberObj = Obj.GetComponent<NumberNode>();
		NumberObj.SetPositionX ( fPositionX );
		NumberObj.SetPositionY ( 0.0f );
		NumberObj.SetPositionZ ( 0.0f );
		NumberObj.SetScaleX ( 1.0f );
		NumberObj.SetScaleY ( 1.0f );
		NumberObj.SetScaleZ ( 1.0f );
		NumberObj.SetSpriteName ( Name );
		NumberObj.SetSpriteDimensions ( 18, 26 );
		NumberObj.SetSpriteColor ( Color.white );
		NumberObj.SetSpriteDepth (22);
		NumberObj.SetNumber ( nNum );
		NumberObj.SetID ( nID );

	}

	public void		SetDiscountData(){

		if( ValueText == "" )	fDiscount = 0.0f;
		
		fDiscount = float.Parse ( ValueText );
		fDiscount /= 100;

	}

	public void		SetActiveNumber(){

		for( int i = 0 ; i < nRankMax ; i++ ){
			if( nRank > i ){
				
				transform.GetChild (i).GetComponent<NumberNode>().gameObject.SetActive( true );
				
			} else {
				
				transform.GetChild (i).GetComponent<NumberNode>().gameObject.SetActive( false );
				
			}
		}

	}

	public void		NumberSpriteUpdate(){
		
		for( int i = nRank ; i > 0 ; i-- ){
			
			transform.GetChild (i).GetComponent<UISprite>().spriteName = transform.GetChild (i-1).GetComponent<UISprite>().spriteName;
			
		}
		
	}


	public void		Sgn_sale_Process(){

		if (nRank >= nRankMax)	return;

		if( nRank == 0 ){
			if( InputValueText == "Btn_00" )	return;
		}

		if (nRank == 1) {
			if( transform.GetChild (0).GetComponent<NumberNode>().GetComponent<UISprite>().spriteName == "num_s_00" ){
				if( InputValueText == "Btn_00" ){
					
					return;
					
				} else {
					
					ValueText 	= "";
					nRank 		= 0;
					
				}
			}
		}

		NumberSpriteUpdate ();

		switch (InputValueText) {

		case	"Btn_00":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_s_00";

			ValueText+= "0";
			nRank++;

			break;

		case	"Btn_01":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_s_01";
			
			ValueText+= "1";
			nRank++;

			break;

		case	"Btn_02":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_s_02";
			
			ValueText+= "2";
			nRank++;

			break;

		case	"Btn_03":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_s_03";
			
			ValueText+= "3";
			nRank++;

			break;

		case	"Btn_04":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_s_04";
			
			ValueText+= "4";
			nRank++;

			break;

		case	"Btn_05":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_s_05";
			
			ValueText+= "5";
			nRank++;

			break;

		case	"Btn_06":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_s_06";
			
			ValueText+= "6";
			nRank++;

			break;

		case	"Btn_07":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_s_07";
			
			ValueText+= "7";
			nRank++;

			break;

		case	"Btn_08":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_s_08";
			
			ValueText+= "8";
			nRank++;

			break;

		case	"Btn_09":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_s_09";
			
			ValueText+= "9";
			nRank++;

			break;

		}

		if (InputValueText == "") {

			return;

		} else {

			SetDiscountData ();

			Wnd_main	Wnd_mainData = GameObject.Find ("Panel").transform.FindChild ("Wnd_main").GetComponent<Wnd_main>();
			Wnd_mainData.SetValueData ();
			Wnd_mainData.SetSpriteName ();
			Wnd_mainData.SetActiveNumber ();
			
			InputValueText = "";

		}

	}

}
