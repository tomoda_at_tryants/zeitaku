﻿using UnityEngine;
using System.Collections;

public class HistoryNode : MonoBehaviour {

	public Vector3		positionData;
	public Vector3		scaleData;
	public int			nRank;
	public int			nRankMax;
	public int			nID;
	public float[]		fPositionX;
	public string[]		SpriteName;
	public char[]		NumberName;
	public int			nValue;
	public int			nTaxValue;
	public bool			fTouch;
	public string		ValueText;
	public GameObject	NumberPrefab;
	public GameObject	HistoryDelPrefab;

	public void			SetPositionX( float fX ){ positionData.x = fX; }
	public void			SetPositionY( float fY ){ positionData.y = fY; }
	public void			SetPositionZ( float fZ ){ positionData.z = fZ; }
	public void			SetScaleX( float fX ){ scaleData.x = fX; }
	public void			SetScaleY( float fY ){ scaleData.y = fY; }
	public void			SetScaleZ( float fZ ){ scaleData.z = fZ; }
	public void			SetID( int nNum ){ nID = nNum; }
	public void			SetRank( int nNum ){ nRank = nNum; }
	public void			SetValue( int nData ){ nValue = nData; }
	public void			SetTaxValue( int nData ){ nTaxValue = nData; }
	public void			SetValueText ( string Text ){ ValueText = Text; }
	public void			SetName( string name ){ transform.name = name; }

	// Use this for initialization
	void Start () {
	
		HistoryNode_Init ();
		CreateNumberObject ();

	}

	public void			HistoryNode_Init(){

		transform.localPosition = positionData;
		transform.localScale 	= scaleData;
		nRankMax 				= 6;
		fPositionX 				= new float[]{ 46.0f, 28.0f, 10f, -8.0f, -26.0f, -44.0f };
		SpriteName 				= new string[]{ "num_s_00", "num_s_01", "num_s_02", "num_s_03", "num_s_04", "num_s_05", "num_s_06", "num_s_07", "num_s_08", "num_s_09", "num_s_10" };
		NumberName 				= new char[]{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.' };
		fTouch 					= false;

	}

	public void			HistoryNode_Reset(){

		for( int i = 0 ; i < transform.childCount ; i++ ){
			if( transform.GetChild(i).name == "Number(Clone)" ){
	
				NumberNode	NumberObj = transform.GetChild (i).GetComponent<NumberNode>();
				UISprite	SpriteObj = transform.GetChild (i).GetComponent<UISprite>();

				SpriteObj.spriteName = "num_s_00";
				NumberObj.SetNumber ( 0 );
				NumberObj.SetActive ( false );

			} else if( transform.GetChild (i).name == "HistoryDelNode" ) {

				transform.GetChild (i).gameObject.SetActive( false );

			}
		}

		nRank 					= 1;
		nValue 					= 0;
		nTaxValue 				= 0;
		ValueText 				= "";
		fTouch 					= false;
		transform.localPosition = positionData;
		transform.localScale 	= scaleData;
		nRankMax 				= 6;
		SpriteName 				= new string[]{ "num_s_00", "num_s_01", "num_s_02", "num_s_03", "num_s_04", "num_s_05", "num_s_06", "num_s_07", "num_s_08", "num_s_09", "num_s_10" };
		NumberName 				= new char[]{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.' };

		int			nlayoutPtnNum = GameObject.Find ("Panel").GetComponent<Panel> ().nlayoutPtnNum;
		string[] 	nameData = new string[]{

			"sgn_history_l", "sgn2_history_l", "sgn3_history_l"

		};
		transform.FindChild ("Background").GetComponent<UISprite> ().spriteName = nameData[nlayoutPtnNum];

	}

	public void			CreateNumberObject(){

		for( int i = 0 ; i < nRankMax ; i++ ){

			CreateNumber("num_s_00", 0, fPositionX[i]);

		}

	}
	
	// Update is called once per frame
	void Update () {
	
		ValueCheck ();
		SetActiveNumber ();
		SetSpriteName ();

	}

	public void			CreateNumber( string Name, int nNum, float PositionX ){

		GameObject Obj = Instantiate (NumberPrefab) as GameObject;
		Obj.transform.parent = transform;

		NumberNode NumberObj = Obj.GetComponent<NumberNode> ();
		NumberObj.SetPositionX ( PositionX );
		NumberObj.SetPositionY ( 0.0f );
		NumberObj.SetPositionZ ( 0.0f );
		NumberObj.SetScaleX ( 1.0f );
		NumberObj.SetScaleY ( 1.0f );
		NumberObj.SetScaleZ ( 1.0f );
		NumberObj.SetSpriteName ( Name );
		NumberObj.SetSpriteDimensions ( 16, 24 );
		NumberObj.SetSpriteColor ( Color.black );
		NumberObj.SetSpriteDepth (19);
		NumberObj.SetNumber ( nNum );
		NumberObj.SetActive ( false );

	}

	public void			CreateHistoryDel(){
	
		GameObject Obj = Instantiate (HistoryDelPrefab) as GameObject;
		Obj.transform.parent = transform;

		HistoryDelNode	DelObj = Obj.GetComponent<HistoryDelNode>();
		DelObj.SetPositionX (  -69.0f );
		DelObj.SetPositionY ( transform.position.y );
		DelObj.SetPositionZ ( -0.1f );
		DelObj.SetScaleX ( 1.0f );
		DelObj.SetScaleY ( 1.0f );
		DelObj.SetScaleZ ( 1.0f );
		DelObj.SetName ( "HistoryDelNode" );
		DelObj.gameObject.SetActive ( false );

	}

	public void		SetSpriteName(){

		UISprite 	Number;
		int 		nIndex = 0;

		for( int i = 0 ; i < transform.childCount ; i++ ){
			if( transform.GetChild ( (transform.childCount-1)-i).name != "Background" &&
			    transform.GetChild ( (transform.childCount-1)-i).name != "HistoryDelNode" ){
				
				Number = transform.GetChild ((transform.childCount-1)-i).GetComponent<UISprite>();

				if(transform.GetChild ((transform.childCount-1)-i).GetComponent<NumberNode>().gameObject.activeSelf == true){
					for( int j = 0 ; j < NumberName.Length ; j++ ){

						if( ValueText[nIndex] == NumberName[j] ){
						
							Number.spriteName = SpriteName[j];
							nIndex++;

							break;

						}
					}
				}

			} 
		}

	}

	public void		SetActiveNumber(){

		for( int i = 0 ; i < transform.childCount ; i++ ){
			if( transform.GetChild (i).name == "Background" )		continue;
			if( transform.GetChild (i).name == "HistoryDelNode" )	continue;

			if( nRank+2 > i ){

				transform.GetChild (i).GetComponent<NumberNode>().gameObject.SetActive (true);

			} else {

				transform.GetChild (i).GetComponent<NumberNode>().gameObject.SetActive (false);

			}
		}

	}

	public void		ValueCheck(){

		if (nValue > 999999)	nValue = 999999;
		if (nTaxValue > 999999) {

			nTaxValue 	= 999999;
			ValueText 	= nTaxValue.ToString ();
			nRank 		= ValueText.Length;
		
		}

	}

	public void		ValueUpdate(){

		if (nValue == 0)		return;
		if (nTaxValue == 0)		return;

		if (Sgn_tax.fTax != 0.0f) {

			float	fValue 	= (float)nValue * Sgn_tax.fTax;
			nTaxValue 		= nValue + Mathf.FloorToInt (fValue);

		} else {

			nTaxValue = nValue;

		}

		ValueText 	= nTaxValue.ToString ();
		nRank 		= ValueText.Length;

	}

}