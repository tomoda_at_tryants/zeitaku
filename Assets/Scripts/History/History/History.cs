﻿using UnityEngine;
using System.Collections;

public enum HISTORY_SIZE
{

	SMALL,
	LONG

}

public class History : MonoBehaviour {
	
	public int			nHistoryCountMax;
	public int			nAddHistoryCount;
	public int			nValue;
	public float[]		fPosX;
	public float[]		fHistoryPosX;
	public GameObject	HistoryPrefab;


	// Use this for initialization
	void Start () {
	
		History_Init ();
		CreateHistoryObject ();

	}

	public void		History_Init(){

		nAddHistoryCount 	= 0;
		nHistoryCountMax 	= 100;
		nValue 				= 0;
		fPosX 				= new float[2]{ 0.0f, 462.0f };
		fHistoryPosX 		= new float[2]{ -230.0f, -232.0f };

		int nReversePtnNum = PlayerPrefs.GetInt ("ReversePtnNum");
		transform.localPosition = new Vector3 ( fPosX[nReversePtnNum], 0.0f, 0.0f  );

	}

	public void		History_Reset(){

		for( int i = 0 ; i < nHistoryCountMax ; i++ ){
			
			HistoryNode	History = transform.GetChild (i).GetComponent <HistoryNode>();
			History.HistoryNode_Reset ();
			
		}

		nAddHistoryCount 	= 0;
		nHistoryCountMax 	= 100;
		nValue 				= 0;

		transform.GetComponent<UIPanel> ().clipOffset 	= new Vector2 ( 97.0f, -109.0f );

		int nReversePtnNum 		= GameObject.Find ("Panel").GetComponent<Panel> ().nReversePtnNum;
		transform.localPosition = new Vector3( fPosX[nReversePtnNum], 0.0f, 0.0f );

	}

	// Update is called once per frame
	void Update () {

		SetActiveHistory ();
		SetSpriteNameUpdate ();

	}

	public void		SetSpriteNameUpdate(){

		int 		nlayoutPtnNum = GameObject.Find ("Panel").GetComponent<Panel> ().nlayoutPtnNum;
		string[] nameData = new string[]{
			
			"sgn_history_l", "sgn2_history_l", "sgn3_history_l"
			
		};
		for( int i = 0 ; i < nHistoryCountMax ; i++ ){
			
			transform.GetChild (i).FindChild ("Background").GetComponent<UISprite>().spriteName = nameData[nlayoutPtnNum];
			
		}

	}

	public void		CreateHistoryObject(){

		int nReversePtnNum = PlayerPrefs.GetInt ("ReversePtnNum");

		for( int i = 0 ; i < nHistoryCountMax ; i++ ){

			GameObject Obj = Instantiate (HistoryPrefab) as GameObject;
			Obj.transform.parent = transform;

			HistoryNode		HistoryObj = Obj.GetComponent<HistoryNode>();
			HistoryObj.SetPositionX ( fHistoryPosX[nReversePtnNum] );
			HistoryObj.SetPositionY ( 258.0f - ( 56.0f * i ) );
			HistoryObj.SetPositionZ ( 0.0f );
			HistoryObj.SetScaleX ( 1.0f );
			HistoryObj.SetScaleY ( 1.0f );
			HistoryObj.SetScaleZ ( 1.0f );
			HistoryObj.SetRank ( 1 );
			HistoryObj.SetID ( i );
			HistoryObj.SetValue( 0 );
			HistoryObj.SetValueText( "" );
			HistoryObj.SetName ( "HistoryNode" );

			HistoryObj.CreateHistoryDel ();

		}

	}

	public void		SetActiveHistory(){

		bool 	fCallSgn_tax 	= GameObject.Find ("TouchEvent").GetComponent<TouchEvent> ().fCallSgn_tax;
		bool 	fCallSgn_sale 	= GameObject.Find ("TouchEvent").GetComponent<TouchEvent> ().fCallSgn_sale;

		for( int i = 0 ; i < nHistoryCountMax ; i++ ){
			if( fCallSgn_tax == true || fCallSgn_sale == true ){

				transform.GetChild (i).GetComponent<HistoryNode>().gameObject.SetActive( false );

			} else {
				if( nAddHistoryCount > i ){

					transform.GetChild (i).GetComponent<HistoryNode>().gameObject.SetActive( true );

				} else {

					transform.GetChild (i).GetComponent<HistoryNode>().gameObject.SetActive( false );

				}
			}
		}

	}

	public void		SetHistoryData(){

		Wnd_main	Wnd_mainObj = GameObject.Find ( "Wnd_main" ).GetComponent<Wnd_main>();
		Wnd_tax		Wnd_taxObj 	= GameObject.Find ("Wnd_tax").GetComponent<Wnd_tax> ();
		HistoryNode HistoryObj 	= transform.GetChild (0).GetComponent<HistoryNode> ();
	
		HistoryObj.SetRank ( Wnd_taxObj.nRank );
		HistoryObj.SetValue ( Wnd_mainObj.nValueData );
		HistoryObj.SetTaxValue ( Wnd_taxObj.nTaxValue );
		HistoryObj.SetValueText ( Wnd_taxObj.ValueText );
	
	}

	public void		HistoryUpdate(){
	
		for( int i = nAddHistoryCount-1 ; i > 0 ; i-- ){

			HistoryNode HistoryObj0 = transform.GetChild (i).GetComponent<HistoryNode> ();
			HistoryNode HistoryObj1 = transform.GetChild (i-1).GetComponent<HistoryNode> ();

			HistoryObj0.nValue 		= HistoryObj1.nValue;
			HistoryObj0.nTaxValue 	= HistoryObj1.nTaxValue;
			HistoryObj0.ValueText 	= HistoryObj1.ValueText;
			HistoryObj0.nRank 		= HistoryObj1.nRank;

		}

	}
	
	public void		HistoryDelete( int nID ){

		if( nAddHistoryCount-1 != nID ){
			for( int i = nID ; i < nAddHistoryCount-1 ; i++ ){

				HistoryNode HistoryObj0 = transform.GetChild (i).GetComponent<HistoryNode> ();
				HistoryNode HistoryObj1 = transform.GetChild (i+1).GetComponent<HistoryNode> ();

				HistoryObj0.HistoryNode_Reset();

				HistoryObj0.nValue 		= HistoryObj1.nValue;
				HistoryObj0.nTaxValue 	= HistoryObj1.nTaxValue;
				HistoryObj0.ValueText 	= HistoryObj1.ValueText;
				HistoryObj0.nRank 		= HistoryObj1.nRank;

			}

			HistoryNode DeleteHistory = transform.GetChild (nAddHistoryCount-1).GetComponent<HistoryNode> ();
			DeleteHistory.HistoryNode_Reset ();
			DeleteHistory.gameObject.SetActive ( false );

		} else {

			HistoryNode DeleteHistory = transform.GetChild (nAddHistoryCount-1).GetComponent<HistoryNode> ();
			DeleteHistory.HistoryNode_Reset ();
			DeleteHistory.gameObject.SetActive ( false );

		}

		if (nAddHistoryCount > 0)	nAddHistoryCount--;

		TotalValue	TotalObj = GameObject.Find ("TotalValue").transform.GetComponent<TotalValue>();
		TotalObj.TotalValue_Update ();
		TotalObj.SetActiveNumber ();
		TotalObj.SetSpriteName ();

		TotalTaxValue	TotalTaxObj = GameObject.Find ("TotalTaxValue").transform.GetComponent<TotalTaxValue>();
		TotalTaxObj.TotalTaxValue_Update ();
		TotalTaxObj.SetActiveNumber ();
		TotalTaxObj.SetSpriteName ();
	
	}

	public void		HistorySetDefault(){

		for( int i = 0 ; i < nAddHistoryCount ; i++ ){
			
			HistoryNode	Obj	 = GameObject.Find ("History").transform.GetChild (i).GetComponent<HistoryNode>();
			
			if( Obj.fTouch == true ){

				Obj.fTouch = false;
				Obj.transform.FindChild("HistoryDelNode").gameObject.SetActive ( false );
				
				return;
				
			}
			
		}


	}

	public void		Reverse(){

		int nReversePtnNum = GameObject.Find ("Panel").GetComponent<Panel> ().nReversePtnNum;
	
		for( int i = 0 ; i < transform.childCount ; i++ ){

			Vector3		localPositionData = transform.GetChild (i).transform.localPosition;

			localPositionData = new Vector3( fHistoryPosX[nReversePtnNum], localPositionData.y, localPositionData.z );
			transform.GetChild (i).transform.localPosition 					= localPositionData;
			transform.GetChild(i).GetComponent<HistoryNode>().positionData 	= localPositionData;

		}

		transform.localPosition = new Vector3 ( fPosX[nReversePtnNum], 0.0f, transform.localPosition.z );

		transform.GetComponent<UIPanel> ().clipOffset = new Vector2 ( 97.0f, -109.0f );

	}
	
}
