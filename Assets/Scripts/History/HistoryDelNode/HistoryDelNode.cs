﻿using UnityEngine;
using System.Collections;

public class HistoryDelNode : MonoBehaviour {

	public Vector3		positionData;
	public Vector3		scaleData;

	public void		SetPositionX( float fX ){ positionData.x = fX; }
	public void		SetPositionY( float fY ){ positionData.y = fY; }
	public void		SetPositionZ( float fZ ){ positionData.z = fZ; }
	public void		SetScaleX( float fX ){ scaleData.x = fX; }
	public void		SetScaleY( float fY ){ scaleData.y = fY; }
	public void		SetScaleZ( float fZ ){ scaleData.z = fZ; }
	public void		SetName( string name ){ transform.name = name; }
	public void		SetActive( bool flag ){ gameObject.SetActive (flag); }
	
	// Use this for initialization
	void Start () {

		HistoryDelNode_Init ();
	
	}

	public void		HistoryDelNode_Init(){

		transform.localPosition = positionData;
		transform.localScale 	= scaleData;

	}

	// Update is called once per frame
	void Update () {


	}

}
