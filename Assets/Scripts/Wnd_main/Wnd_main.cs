﻿using UnityEngine;
using System.Collections;

public class Wnd_main : MonoBehaviour {
	
	public int			nRank;
	public int			nRankMax;
	public int			nValueData;
	public int			nValueDataBuf;
	public float[][]	fPositionX;
	public string		ValueText;
	public string		InputValueText;
	public string[]		SpriteNameData;
	public char[]		NumberName;
	public GameObject	NumberPrefab;

	// Use this for initialization
	void Start () {
	
		Wnd_main_Init ();
		CreateNumberObject ();

	}

	// Update is called once per frame
	void Update () {

		SetActiveNumber ();
		
	}

	public void		Wnd_main_Init(){

		nRank 				= 1;
		nRankMax 			= 6;
		nValueData 			= 0;
		nValueDataBuf 		= 0;
		ValueText 			= "0";
		fPositionX 			= new float[][]{ 
		
			new[]{		//	Right

				235.0f, 201.0f, 167.0f, 133.0f, 99.0f, 65.0f 

			},

			new[]{		//	Left

				-77.0f, -111.0f, -145.0f, -179.0f, -213.0f, -247.0f

			}
		
		};

		SpriteNameData = new string[]{

			"num_l_00", "num_l_01", "num_l_02", "num_l_03", "num_l_04", "num_l_05", "num_l_06", "num_l_07", "num_l_08", "num_l_09"

		};
		NumberName 		= new char[]{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.' };


	}

	public void		Wnd_main_Reset(){

		for( int i = 0 ; i < nRank ; i++ ){

			transform.GetChild (i).gameObject.SetActive ( false );
			transform.GetChild (i).GetComponent<UISprite>().spriteName = "num_l_00";
			transform.GetChild (i).GetComponent<NumberNode>().nNumber = 0;
			
		}
		nRank 				= 1;
		nRankMax 			= 6;
		nValueData 			= 0;
		nValueDataBuf 		= 0;
		ValueText 			= "0";

	}

	public void		CreateNumberObject(){

		int nReversePtnNum = PlayerPrefs.GetInt ("ReversePtnNum");

		for( int i = 0 ; i < nRankMax ; i++ ){

			CreateNumber ( "num_l_00", 0 );
			transform.GetChild(i).GetComponent<NumberNode>().positionData.x = fPositionX[nReversePtnNum][i];
			
		}

	}
	
	public void		Wnd_main_Process(){

		if (nRank >= nRankMax)	return;

		if( nRank == 1 ){
			if( transform.GetChild (0).GetComponent<NumberNode>().GetComponent<UISprite>().spriteName == "num_l_00" ){
				if( InputValueText == "Btn_00" )	return;

				if( InputValueText != "Btn_00" ){
					
					ValueText	= "";
					nRank 		= 0;
					
				}
			}

		}

		if( GameObject.Find ("Wnd_tax").GetComponent<Wnd_tax>().nTaxValue >= 999999 ){

			return;

		}

		NumberSpriteUpdate ();

		switch (InputValueText) {
			
		case	"Btn_00":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_l_00";
			transform.GetChild (0).GetComponent<NumberNode>().nNumber = 0;

			ValueText += "0";

			nRank++;
			
			break;
			
		case	"Btn_01":
		
			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_l_01";
			transform.GetChild (0).GetComponent<NumberNode>().nNumber = 1;

			ValueText += "1";

			nRank++;
			
			break;
			
		case	"Btn_02":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_l_02";
			transform.GetChild (0).GetComponent<NumberNode>().nNumber = 2;

			ValueText += "2";

			nRank++;
			
			break;
			
		case	"Btn_03":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_l_03";
			transform.GetChild (0).GetComponent<NumberNode>().nNumber = 3;

			ValueText += "3";

			nRank++;
			
			break;
			
		case	"Btn_04":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_l_04";
			transform.GetChild (0).GetComponent<NumberNode>().nNumber = 4;

			ValueText += "4";

			nRank++;
			
			break;
			
		case	"Btn_05":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_l_05";
			transform.GetChild (0).GetComponent<NumberNode>().nNumber = 5;

			ValueText += "5";

			nRank++;
			
			break;
			
		case	"Btn_06":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_l_06";
			transform.GetChild (0).GetComponent<NumberNode>().nNumber = 6;

			ValueText += "6";

			nRank++;
			
			break;
			
		case	"Btn_07":

			ValueText += "7";

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_l_07";
			transform.GetChild (0).GetComponent<NumberNode>().nNumber = 7;

			nRank++;
			
			break;
			
		case	"Btn_08":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_l_08";
			transform.GetChild (0).GetComponent<NumberNode>().nNumber = 8;

			ValueText += "8";

			nRank++;
			
			break;
			
		case	"Btn_09":

			transform.GetChild (0).GetComponent<UISprite>().spriteName = "num_l_09";
			transform.GetChild (0).GetComponent<NumberNode>().nNumber = 9;

			ValueText += "9";

			nRank++;
			
			break;
			
		}

		SetValueData ();
		
	}
	
	public void		CreateNumber( string Name, int nNum ){
		
		GameObject	Obj 		= Instantiate (NumberPrefab) as GameObject;
		Obj.transform.parent 	= GameObject.Find ("Wnd_main").transform;
		
		NumberNode NumberObj = Obj.GetComponent<NumberNode> ();
		NumberObj.SetPositionX ( 0.0f );
		NumberObj.SetPositionY ( -3.0f );
		NumberObj.SetPositionZ ( 0.0f );
		NumberObj.SetScaleX ( 1.0f );
		NumberObj.SetScaleY ( 1.0f );
		NumberObj.SetScaleZ ( 1.0f );
		NumberObj.SetSpriteName ( Name );
		NumberObj.SetSpriteDimensions ( 30, 38 );
		NumberObj.SetSpriteColor ( Color.white );
		NumberObj.SetSpriteDepth (3);
		NumberObj.SetNumber ( nNum );
		
	}

	public void		SetValueData(){

		if (ValueText == "") {

			nValueData = 0;
			return;

		}

		nValueData = int.Parse (ValueText);

		if( Sgn_sale.fDiscount != 0.0f ){

			nValueData = nValueDataBuf;
			float	fValue = nValueDataBuf * Sgn_sale.fDiscount;
			nValueData = nValueData - AssessmentProcess ( fValue );
			ValueText = nValueData.ToString();
			nRank = ValueText.Length;

		} else {

			nValueData = int.Parse ( ValueText );

		}

	}

	public void		SetActiveNumber(){

		for( int i = 0 ; i < nRankMax ; i++ ){
			if( nRank > i ){

				transform.GetChild (i).GetComponent<NumberNode>().gameObject.SetActive ( true );

			} else {

				transform.GetChild (i).GetComponent<NumberNode>().gameObject.SetActive ( false );

			}
		}

	}

	public void		NumberSpriteUpdate(){

		for( int i = nRank ; i > 0 ; i-- ){

			transform.GetChild (i).GetComponent<UISprite>().spriteName = transform.GetChild (i-1).GetComponent<UISprite>().spriteName;
			transform.GetChild (i).GetComponent<NumberNode>().nNumber = transform.GetChild (i-1).GetComponent<NumberNode>().nNumber;

		}

	}

	public void		NumberPositionUpdate(){

		int	nReversePtnNum = GameObject.Find ("Panel").GetComponent<Panel>().nReversePtnNum;

		for( int i = 0 ; i < transform.childCount ; i++ ){

			Vector3	UpdatePos = transform.GetChild (i).localPosition;

			UpdatePos = new Vector3( fPositionX[nReversePtnNum][i], UpdatePos.y, UpdatePos.z );

			transform.GetChild (i).localPosition = UpdatePos;

		}

	}

	public int		AssessmentProcess( float fValue ){
		
		fValue *= 10.0f;
		fValue = Mathf.Floor ( fValue );
		fValue /= 10.0f;
		
		int nValue = Mathf.FloorToInt ( fValue );
		
		if (fValue > nValue)	nValue += 1;
		
		return		nValue;
		
	}

	public void		SetSpriteName(){

		for( int i = 0 ; i < nRank ; i++ ){
			
			UISprite	Number = transform.GetChild ((nRank-1) - i).GetComponent<UISprite>();
			
			for( int j = 0 ; j < NumberName.Length ; j++ ){
				if( ValueText[i] == NumberName[j] )	Number.spriteName = SpriteNameData[j];
			}
			
		}

	}

}
