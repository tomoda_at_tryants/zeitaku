﻿using UnityEngine;
using System.Collections;

public enum LAYOUT_PTN{

	PTN0, PTN1, PTN2

}

public enum REVERSE_PTN{

	RIGHT, LEFT

}

public class Panel : MonoBehaviour {
	
	public int				nlayoutPtnNum;
	public int				nReversePtnNum;
	public float[][]		fWnd_taxPositionX;
	public bool[]			fChild;
	public bool[]			fButton;
	public bool[]			fSprite;
	public string[]			nameData;
	public string[][]		spriteNameData;
	public string[]			BGSpriteNameData;
	public string[]			BgMaskSpriteNameData;
	public Vector3[][]		positionData;
	public Color[][]		colorData;

	public int			nFrameCount = 0;
	public int 			nTouchCount = 0;
	public int 			nFrameMax 	= 60;
	public bool			fTouch 		= false;
	
	// Use this for initialization
	void Start () {

		nReversePtnNum 					= PlayerPrefs.GetInt ( "ReversePtnNum" ); 
		nlayoutPtnNum 					= PlayerPrefs.GetInt ( "LayoutPtnNum" );

		TotalTaxValue	TotalTaxData 	= GameObject.Find ("TotalTaxValue").GetComponent<TotalTaxValue>();	
		TotalTaxData.SetNumberColor ();
		TotalValue		TotalData 		= GameObject.Find ("TotalValue").GetComponent<TotalValue>();	
		TotalData.SetNumberColor ();
		Parenthesis		ParenthesisData = GameObject.Find ("Parenthesis").GetComponent<Parenthesis>();	

		ParenthesisData.SetParenthesisColor ();

		fWnd_taxPositionX = new float[][]{
		
			new[]{ -160.0f, -160.0f, -160.0f },
			new[]{ 149.0f, 153.0f, 160.0f }

		};
		BGSpriteNameData = new string[]{

			"bg1b", "bg2b", "bg3b"
		
		};
		BgMaskSpriteNameData = new string[]{

			"bgmask1", "bgmask2", "bgmask3", "bgmask4"

		};

		spriteNameData = new string[][]{

			new[]{
		
				"bg1a", "bgmask", "btn_00", "btn_01", "btn_02", "btn_03", "btn_04", "btn_05", "btn_06", "btn_07", "btn_08", "btn_09", "btn_13", "btn_11", "btn_1del",
				"btn_d1",  "btn_d2", "btn_d3", "btn_function4", "btn_reset", "btn_tax", "sgn_history_l", "", "sgn_over","sgn_sale", "sgn_tax", "", "", "wnd_main", "wnd_tax"

			},

			new[]{
				
				"bg2a", "bgmask", "btn2_00", "btn2_01", "btn2_02", "btn2_03", "btn2_04", "btn2_05", "btn2_06", "btn2_07", "btn2_08", "btn2_09", "btn2_13", "btn2_11", "btn2_1del",
				"btn_d1", "btn_d2", "btn_d3", "btn2_function4", "btn2_reset", "btn2_tax", "sgn2_history_l", "", "sgn_over","sgn2_sale", "sgn2_tax", "", "", "wnd2_main", "wnd2_tax"
					
			},

			new[]{
				
				"bg3a", "bgmask", "btn3_00", "btn3_01", "btn3_02", "btn3_03", "btn3_04", "btn3_05", "btn3_06", "btn3_07", "btn3_08", "btn3_09", "btn3_13", "btn3_11", "btn3_1del",
				"btn_d1", "btn_d2", "btn_d3", "btn3_function4", "btn3_reset", "btn3_tax", "sgn3_history_l", "", "sgn_over","sgn3_sale", "sgn3_tax", "", "", "wnd3_main", "wnd3_tax"
					
			}

		};

		positionData = new Vector3[][]{

			new[]{		//	Right

				new Vector3(0.0f,0.0f,0.0f), 		new Vector3(0.0f,0.0f,0.0f),		new Vector3(86.0f,-250.0f,0.0f), 	new Vector3(-64.0f,-100.0f,0.0f), 	
				new Vector3(86.0f,-100.0f,0.0f), 	new Vector3(237.0f,-100.0f,0.0f), 	new Vector3(-64.0f,50.0f,0.0f), 	new Vector3(86.0f,50.0f,0.0f), 	
				new Vector3(237.0f,50.0f,0.0f), 	new Vector3(-64.0f,200.0f,0.0f), 	new Vector3(86.0f,200.0f,0.0f), 	new Vector3(237.0f,200f,0.0f), 	
				new Vector3(-64.0f,-250.0f,0.0f),	new Vector3(237.0f,-250.0f,0.0f), 	new Vector3(280.0f,339.0f,0.0f), 	new Vector3(40.0f,435.0f,0.0f), 	
				new Vector3(120.0f,435.0f,0.0f), 	new Vector3(200.0f,435.0f,0.0f), 	new Vector3(280.0f,435.0f,0.0f), 	new Vector3(-270.0f,433.0f,0.0f), 	
				new Vector3(-40.0f,435.0f,0.0f), 	new Vector3(0.0f,0.0f,0.0f),		new Vector3( 0.0f,0.0f,0.0f ),	 	new Vector3(-155.0f,340.0f,0.0f),	
				new Vector3(209.0f,435.0f,0.0f),	new Vector3(-120.0f,435.0f,0.0f),	new Vector3(0.0f,0.0f,0.0f), 		new Vector3(0.0f,0.0f,0.0f),		
				new Vector3(0.0f,342.0f,0.0f), 		new Vector3(-160.0f,342.0f,0.0f)

			},

			new[]{		//	Left

				new Vector3(0.0f,0.0f,0.0f), 		new Vector3(0.0f,0.0f,0.0f),		new Vector3(-85.0f,-250.0f,0.0f), 	new Vector3(-235.0f,-100.0f,0.0f), 	
				new Vector3(-85.0f,-100.0f,0.0f), 	new Vector3(66.0f,-100.0f,0.0f), 	new Vector3(-235.0f,50.0f,0.0f), 	new Vector3(-85.0f,50.0f,0.0f), 	
				new Vector3(66.0f,50.0f,0.0f), 		new Vector3(-235.0f,200.0f,0.0f), 	new Vector3(-85.0f,200.0f,0.0f), 	new Vector3(66.0f,200f,0.0f), 	
				new Vector3(-235.0f,-250.0f,0.0f),	new Vector3(66.0f,-250.0f,0.0f), 	new Vector3(-32.0f,339.0f,0.0f), 	new Vector3(40.0f,435.0f,0.0f), 	
				new Vector3(120.0f,435.0f,0.0f), 	new Vector3(200.0f,435.0f,0.0f), 	new Vector3(280.0f,435.0f,0.0f), 	new Vector3(-270.0f,433.0f,0.0f), 	
				new Vector3(-40.0f,435.0f,0.0f), 	new Vector3(0.0f,0.0f,0.0f),		new Vector3(0.0f,0.0f,0.0f), 		new Vector3(154.0f,340.0f,0.0f),	
				new Vector3(209.0f,435.0f,0.0f),	new Vector3(-120.0f,435.0f,0.0f),	new Vector3(0.0f,0.0f,0.0f), 		new Vector3(0.0f,0.0f,0.0f),		
				new Vector3(0.0f,342.0f,0.0f), 		new Vector3(149.0f,342.0f,0.0f)

			}

		};

		nameData = new string[]{
			
			"BG", "Bgmask", "Btn_00", "Btn_01", "Btn_02", "Btn_03", "Btn_04", "Btn_05", "Btn_06", "Btn_07", "Btn_08","Btn_09", "Btn_10", "Btn_11",  "Btn_del",
			"Btn_function1", "Btn_function2", "Btn_function3", "Btn_function4", "Btn_reset", "Btn_tax","History", "Parenthesis", "Sgn_over", "Sgn_sale", "Sgn_tax", 
			"TotalTaxValue", "TotalValue", "Wnd_main", "Wnd_tax"
			
		};

		fChild = new bool[]{

			false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false, false, false

		};

		fButton = new bool[]{
			
			false, false, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, false, false, false, false, false, false, false, false
			
		};

		fSprite = new bool[]{  

			true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, true, false, true, true, true, false, false, true, true

		};

		UISprite	Over_TaxSprite = transform.FindChild ("Sgn_over").GetComponent<UISprite> ();
		Over_TaxSprite.SetDimensions ( 150, 50 );
		Over_TaxSprite.color = Color.red;

		SetScreen ();

	}

	public void		Update(){

		Android_End ();
		LayoutUpdate ();
		ChangeSpriteName_Btn_10 ();
		ChangeSpriteName_Btn_11 ();

	}

	void	Awake(){

		AdMob_Init ();

	}

	public void		SetScreen(){
	
		UIRoot	Root 	= transform.parent.GetComponent<UIRoot>();
		float 	ap 		= Screen.height * 1.0f / Screen.width * 1.0f;
		Root.manualHeight = (int)( 640.0f * ap );
	
		float 		fPanelHeight 	= 960.0f;
		float 		fOffsetY 		= ( (Root.manualHeight - fPanelHeight ) / 2);
	
		transform.localPosition = new Vector3 ( 0.0f, fOffsetY, 0.0f );

	}

	public void		Android_End(){
	
		if( fTouch == true ){

			nFrameCount++;

			if( nFrameCount >= nFrameMax ){

				nTouchCount = 0;
				nFrameCount = 0;
				fTouch 		= false;

			}

		}

		if( Application.platform == RuntimePlatform.Android ){
			if( Input.GetKeyDown ( KeyCode.Escape ) ){
				if ( nTouchCount == 0 )	fTouch = true;

				nTouchCount++;

				if( fTouch == true && nTouchCount == 2 )	Application.Quit ();

			}
		}

	}

	public void		AdMob_Init(){
	
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		//	AdMob
		///////////////////////////////////////////////////////////////////////////////////////////////////////////
		#if UNITY_ANDROID
		AdMobAndroid.init( "ca-app-pub-4782481582877192/4666444663" );
		AdMobAndroid.createBanner( AdMobAndroidAd.smartBanner, AdMobAdPlacement.BottomCenter );
		#elif UNITY_IPHONE
		AdMobBinding.init( "ca-app-pub-4782481582877192/7619911062", false );
		AdMobBinding.createBanner( AdMobBannerType.SmartBannerPortrait, AdMobAdPosition.BottomCenter );
		#endif

	}

	public void		LayoutUpdate(){

		int		childCount = transform.childCount;
		
		for( int i = 0 ; i < childCount ; i++ ){
			if( fSprite[i] == true ){
				if( fChild[i] == true ){
					if( nameData[i] == "History" ){
						for( int j = 0 ; j < GameObject.Find("History").transform.childCount ; j++ ){

							HistoryNode	Obj = GameObject.Find ("History").transform.GetChild (j).GetComponent<HistoryNode>();
							Obj.transform.GetChild (0).GetComponent<UISprite>().spriteName = spriteNameData[nlayoutPtnNum][i];

						}
					} else {

						transform.FindChild(nameData[i]).transform.localPosition = positionData[nReversePtnNum][i];
						transform.FindChild(nameData[i]).FindChild ("Background").GetComponent<UISprite>().spriteName = spriteNameData[nlayoutPtnNum][i];

					}	
				} else {
					if( nameData[i] == "BG" ){

						transform.FindChild(nameData[i]).transform.localPosition = positionData[nReversePtnNum][i];

						if( nReversePtnNum == (int)REVERSE_PTN.RIGHT ){

							transform.FindChild(nameData[i]).GetComponent<UISprite>().spriteName 	= spriteNameData[nlayoutPtnNum][i];

						} else {

							transform.FindChild(nameData[i]).GetComponent<UISprite>().spriteName 	= BGSpriteNameData[nlayoutPtnNum];

						}

					} else if( nameData[i] == "Bgmask" ){

						transform.FindChild(nameData[i]).transform.localPosition 				= positionData[nReversePtnNum][i];

						bool	fCallSgn_sale = GameObject.Find("TouchEvent").GetComponent<TouchEvent>().fCallSgn_sale;
						if( fCallSgn_sale == true ){

							transform.FindChild(nameData[i]).GetComponent<UISprite>().spriteName 	= BgMaskSpriteNameData[nReversePtnNum+2];

						} else {

							transform.FindChild(nameData[i]).GetComponent<UISprite>().spriteName 	= BgMaskSpriteNameData[nReversePtnNum];

						}

					} else if( nameData[i] == "Wnd_tax" ){
							
						transform.FindChild(nameData[i]).transform.localPosition = new Vector3( fWnd_taxPositionX[nReversePtnNum][nlayoutPtnNum], positionData[nReversePtnNum][i].y, positionData[nReversePtnNum][i].z );
						transform.FindChild(nameData[i]).GetComponent<UISprite>().spriteName = spriteNameData[nlayoutPtnNum][i];

					} else {

						transform.FindChild(nameData[i]).transform.localPosition 					= positionData[nReversePtnNum][i];
						transform.FindChild(nameData[i]).GetComponent<UISprite>().spriteName 		= spriteNameData[nlayoutPtnNum][i];

					}
				}
			}
		}

	}

	public void		SaveLayoutPtn(){

		PlayerPrefs.SetInt ( "LayoutPtnNum", nlayoutPtnNum );
		PlayerPrefs.Save ();

	}

	public void		SaveReversePtn(){

		PlayerPrefs.SetInt ( "ReversePtnNum", nReversePtnNum );
		PlayerPrefs.Save ();

	}

	public string	GetName( int nNum ){

		if (nNum > nameData.Length)		return	"";

		return	nameData[nNum];

	}

	public bool		GetButtonFlag( int nNum ){

		return	fButton[nNum];
		
	}

	public void		ChangeSpriteName_Btn_10(){

		string[]	Btn_10Name 			= new string[]{ "btn_10", "btn2_10", "btn3_10" };
		string[]	Btn_13Name 			= new string[]{ "btn_13", "btn2_13", "btn3_13" };
		bool 		fDisplayDiscount 	= GameObject.Find ("TouchEvent").GetComponent<TouchEvent> ().fDisplaySale;

		if( fDisplayDiscount == true ){

			transform.FindChild ("Btn_10").FindChild ("Background").GetComponent<UISprite>().spriteName = Btn_13Name[nlayoutPtnNum];

		} else {

			transform.FindChild ("Btn_10").FindChild ("Background").GetComponent<UISprite>().spriteName = Btn_10Name[nlayoutPtnNum];

		}

	}

	public void		ChangeSpriteName_Btn_11(){
		
		string[]	btn_12Name = new string[]{ "btn_12", "btn2_12", "btn3_12" };
		string[]	btn_13Name = new string[]{ "btn_13", "btn2_13", "btn3_13" };
		TouchEvent 	TouchEventData 	= GameObject.Find ("TouchEvent").GetComponent<TouchEvent> ();
	
		if( TouchEventData.fCallSgn_tax == true ){
			
			transform.FindChild("Btn_11").GetChild (0).GetComponent<UISprite>().spriteName = btn_12Name[nlayoutPtnNum];
			
		} 

		if( TouchEventData.fCallSgn_sale == true ){

			transform.FindChild("Btn_11").GetChild (0).GetComponent<UISprite>().spriteName = btn_13Name[nlayoutPtnNum];

		}
		
	}

}
