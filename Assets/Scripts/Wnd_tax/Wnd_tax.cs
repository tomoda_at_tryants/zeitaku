﻿using UnityEngine;
using System.Collections;

public class Wnd_tax : MonoBehaviour{

	public int			nRank;
	public int			nRankMax;
	public int			nTaxValue;
	public float[]		fPositionX;
	public string		ValueText;
	public string[]		SpriteName;
	public char[]		NumberName;
	public bool			fValueOver;
	public GameObject	NumberPrefab;

	// Use this for initialization
	void Start () {
	
		Wnd_tax_Init ();
		CreateNumberObject ();

	}

	// Update is called once per frame
	void Update () {

		if( GameObject.Find ("TouchEvent").GetComponent<TouchEvent>().fCallSgn_tax == false ){

			SetTaxValue ();
			SetValueText ();
			SetRank ();
			SetActiveNumber ();
			SetSpriteName ();
			ValueOverProcess ();

		}
		
	}
	
	public void		Wnd_tax_Init(){

		nRank 			= 1;
		nRankMax 		= 6;
		nTaxValue 		= 0;
		ValueText 		= "";
		fValueOver 		= false;
		fPositionX 		= new float[]{ 115.0f, 71.0f, 27.0f, -17.0f, -61.0f, -105.0f };
		SpriteName 		= new string[]{ "num_l_00", "num_l_01", "num_l_02", "num_l_03", "num_l_04", "num_l_05", "num_l_06", "num_l_07", "num_l_08", "num_l_09", "num_l_10" };
		NumberName 		= new char[]{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '.' };

	}

	public void		Wnd_tax_Reset(){

		for( int i = 0 ; i < nRank ; i++ ){

			transform.GetChild (i).gameObject.SetActive ( false );
			transform.GetChild (i).GetComponent<UISprite>().spriteName = "num_l_00";
			transform.GetChild (i).GetComponent<NumberNode>().nNumber = 0;
			
		}
		nRank 			= 1;
		nRankMax 		= 6;
		nTaxValue 		= 0;
		ValueText 		= "";
		fValueOver 		= false;

	}

	public void		CreateNumberObject(){	

		for (int i = 0; i < nRankMax; i++) {
			
			this.CreateNumber ("num_l_00", 0);
			transform.GetChild (i).GetComponent<NumberNode> ().SetPositionX ( fPositionX [i] );
			
		}

	}

	public void		ValueOverProcess (){

		GameObject	Over_taxObj = GameObject.Find ("Panel").transform.FindChild ("Sgn_over").gameObject;

		int nValue = GameObject.Find ("Wnd_main").GetComponent<Wnd_main> ().nValueData;
		if( nValue == 999999 ){
			if( Sgn_tax.fTax == 0.0f ){

				fValueOver = false;
				Over_taxObj.SetActive ( false );
				return;

			}
		}

		if( nTaxValue >= 999999 ){

			fValueOver = true;

			Over_taxObj.SetActive( true );
			for( int i = 0 ; i < nRankMax ; i++ ){

				transform.GetChild (i).gameObject.SetActive ( false );

			}
			
		} else {

			fValueOver = false;
			Over_taxObj.SetActive( false );
			
		}

	}

	public void		SetTaxValue(){

		if (GameObject.Find ("Wnd_main").GetComponent<Wnd_main> ().nValueData == 0)	{

			nTaxValue = 0;
			return;

		}

		nTaxValue = GameObject.Find ("Wnd_main").GetComponent<Wnd_main> ().nValueData;

		if (Sgn_tax.fTax != 0.0f) {
			
			float	fValue 	= nTaxValue * Sgn_tax.fTax;
			nTaxValue 		= nTaxValue + Mathf.FloorToInt(fValue);
			
		}		

	}
	
	public void		SetValueText(){

		ValueText = nTaxValue.ToString ();
	
	}

	public void		SetRank(){

		if( nTaxValue > 999999 ){
		
			nTaxValue = 999999;
			SetValueText();
			nRank = 6;
			
		}
		if (nRank > nRankMax)	nRank = nRankMax;

		nRank = ValueText.Length;

	}

	public void		SetSpriteName(){

		for( int i = 0 ; i < nRank ; i++ ){

			UISprite	Number = transform.GetChild ((nRank-1) - i).GetComponent<UISprite>();

			for( int j = 0 ; j < NumberName.Length ; j++ ){
				if( ValueText[i] == NumberName[j] )	Number.spriteName = SpriteName[j];
			}

		}

	}

	public void		CreateNumber( string Name, int nNum ){
		
		GameObject	Obj 		= Instantiate (NumberPrefab) as GameObject;
		Obj.transform.parent 	= GameObject.Find ("Wnd_tax").transform;
		
		NumberNode NumberObj = Obj.GetComponent<NumberNode> ();
		NumberObj.SetPositionX ( 0.0f );
		NumberObj.SetPositionY ( -4.0f );
		NumberObj.SetPositionZ ( 0.0f );
		NumberObj.SetScaleX ( 1.0f );
		NumberObj.SetScaleY ( 1.0f );
		NumberObj.SetScaleZ ( 1.0f );
		NumberObj.SetSpriteName ( Name );
		NumberObj.SetSpriteDimensions ( 38, 46 );
		NumberObj.SetSpriteColor ( Color.white );
		NumberObj.SetSpriteDepth (3);
		NumberObj.SetNumber ( nNum );
		
	}

	public void		SetActiveNumber(){

		for( int i = 0 ; i < nRankMax ; i++ ){
			if( nRank > i ){

				transform.GetChild (i).GetComponent<NumberNode>().gameObject.SetActive( true );

			} else {

				transform.GetChild (i).GetComponent<NumberNode>().gameObject.SetActive ( false );

			}
		}

	}

}
