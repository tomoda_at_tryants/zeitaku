﻿using UnityEngine;
using System.Collections;

public class TotalValue : MonoBehaviour {
	
	public int			nRank;
	public int			nRankMax;
	public int			nTotalValue;
	public float[][]	fPositionX;
	public string		TotalValueText;
	public string[]		SpriteName;
	public char[]		NumberName;
	public GameObject	NumberPrefab;
	
	// Use this for initialization
	void Start () {
		
		TotalValue_Init ();
		CreateNumberObject ();
		SetActiveNumber ();
		
	}
	
	public void		TotalValue_Init(){
		
		nRank 				= 1;
		nRankMax 			= 7;
		nTotalValue 		= 0;
		fPositionX 			= new float[][]{ 

			new[]{ -16.0f, -33.0f, -50.0f, -67.0f, -84.0f, -101.0f, -118.0f },
			new[]{ 114.0f, 97.0f, 80.0f, 63.0f, 46.0f, 29.0f, 12.0f }

		};
		TotalValueText 		= "";
		SpriteName 			= new string[]{ "num_s_00", "num_s_01", "num_s_02", "num_s_03", "num_s_04", "num_s_05", "num_s_06", "num_s_07", "num_s_08", "num_s_09", "num_s_10" };
		NumberName 			= new char[]{ '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' };
		
	}
	
	public void		TotalValue_Reset(){
		
		for( int i = 0 ; i < nRank ; i++ ){
			
			NumberNode		Child = transform.GetChild (i).GetComponent<NumberNode>();
			
			if( i == 0 ){
				
				Child.gameObject.SetActive( true );
				
			} else {
				
				Child.gameObject.SetActive( false );
				
			}
		}
		
		nRank 			= 1;
		nRankMax 		= 7;
		nTotalValue 	= 0;
		TotalValueText 	= "0";
		
	}
	
	public void		CreateNumberObject(){

		int nReversePtnNum = PlayerPrefs.GetInt ("ReversePtnNum");

		for( int i = 0 ; i < nRankMax ; i++ ){
			
			CreateNumber ();
			transform.GetChild (i).GetComponent<NumberNode> ().SetPositionX ( fPositionX [nReversePtnNum][i] );
			
		}

	}
	
	public void		CreateNumber (){
		
		GameObject	Obj 		= Instantiate (NumberPrefab) as GameObject;
		Obj.transform.parent 	= GameObject.Find ("TotalValue").transform;
		
		NumberNode NumberObj = Obj.GetComponent<NumberNode> ();
		NumberObj.SetPositionX ( 0.0f );
		NumberObj.SetPositionY ( -362.0f );
		NumberObj.SetPositionZ ( 0.0f );
		NumberObj.SetScaleX ( 1.0f );
		NumberObj.SetScaleY ( 1.0f );
		NumberObj.SetScaleZ ( 1.0f );
		NumberObj.SetSpriteName ( "num_s_00" );
		NumberObj.SetSpriteDimensions ( 15, 23 );
		NumberObj.SetSpriteColor ( Color.black );
		NumberObj.SetSpriteDepth (3);
		NumberObj.SetNumber ( 0 );
		
	}
	
	public void		SetNumberColor(){
		
		Color[]		ColorData 	= new Color[]{ Color.black, Color.white, Color.black };
		int 		PtnNum 		= GameObject.Find ("Panel").GetComponent<Panel> ().nlayoutPtnNum;
		int 		nLoopMax 	= transform.childCount;
		
		for( int i = 0 ; i < nLoopMax ; i++ ){
			
			NumberNode	Obj = transform.GetChild (i).GetComponent<NumberNode>();
			
			Obj.SetSpriteColor ( ColorData[PtnNum] );
			
		}
		
	}
	
	public void		SetActiveNumber(){
		
		for( int i = 0 ; i < nRankMax ; i++ ){
			if( nRank > i ){
				
				transform.GetChild (i).GetComponent<NumberNode>().gameObject.SetActive( true );
				
			} else {
				
				transform.GetChild (i).GetComponent<NumberNode>().gameObject.SetActive ( false );
				
			}
		}
		
	}
	
	public void		TotalValue_Update(){
		
		History		HistoryData = GameObject.Find ("History").GetComponent<History> ();
		
		nTotalValue = 0;
		for( int i = 0 ; i < HistoryData.nAddHistoryCount ; i++ ){
			
			HistoryNode	HistoryNode = GameObject.Find ("History").transform.GetChild (i).GetComponent<HistoryNode>();
			nTotalValue += HistoryNode.nValue;
			
		}
		
		if( nTotalValue > 9999999 )	nTotalValue = 9999999;
		
		TotalValueText 	= nTotalValue.ToString ();
		nRank 			= TotalValueText.Length;
		
	}
	
	public void		SetSpriteName(){
		
		for( int i = 0 ; i < nRank ; i++ ){
			
			UISprite	Number = transform.GetChild (i).GetComponent<UISprite>();
			
			for( int j = 0 ; j < NumberName.Length ; j++ ){
				if( TotalValueText[(nRank-1)-i] == NumberName[j] )	Number.spriteName = SpriteName[j];
			}
			
		}
		
	}

	public void		Reverse(){

		int 	nReversePtnNum = GameObject.Find ("Panel").GetComponent<Panel> ().nReversePtnNum;

		for( int i = 0 ; i < transform.childCount ; i++ ){

			Vector3		UpdatePos = new Vector3( fPositionX[nReversePtnNum][i], -362.0f, 0.0f );
			transform.GetChild (i).GetComponent<NumberNode>().positionData = UpdatePos;
			transform.GetChild (i).localPosition = UpdatePos;

		}

	}

}
