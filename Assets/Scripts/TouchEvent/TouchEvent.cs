﻿using UnityEngine;
using System.Collections;

public class TouchEvent : MonoBehaviour {

	public int				MethodNum;
	public int				nDeleteButtonCount;
	public string			TouchButtonName;
	public static bool		fInput;
	public RaycastHit		HitData;
	public Ray				ray;
	public delegate void	Function();
	public Function[]		TouchFunctionMethod;
	public bool				fCallSgn_tax;
	public bool				fCallSgn_sale;
	public bool				fDisplaySale;

	// Use this for initialization
	void Start () {
	
		TouchEvent_Init ();

	}

	// Update is called once per frame
	void Update () {

		TouchEvent_Main ();
		if (fInput == true)		TouchFunctionMethod [MethodNum] ();

	}

	public void		TouchEvent_Init(){

		MethodNum 			= 0;
		nDeleteButtonCount 	= 0;
		TouchButtonName 	= "";
		fInput 				= false;
		ray 				= new Ray ();
		HitData 			= new RaycastHit ();
		TouchFunctionMethod = new[]{ new Function(TouchFunction_Number), 			new Function(TouchFunction_Number), 		new Function(TouchFunction_Number), 	new Function(TouchFunction_Number), 
									 new Function(TouchFunction_Number), 			new Function(TouchFunction_Number), 		new Function(TouchFunction_Number), 	new Function(TouchFunction_Number), 
									 new Function(TouchFunction_Number), 			new Function(TouchFunction_Number), 		new Function(TouchFunction_Btn_10), 	new Function(TouchFunction_Btn_11), 
									 new Function(TouchFunction_Wnd_main_Delete), 	new Function(TouchFunction_Layout), 		new Function(TouchFunction_Layout), 	new Function(TouchFunction_Layout), 
									 new Function(TouchFunction_Reverse), 			new Function(TouchFunction_Reset), 			new Function(TouchFunction_Tax),		new Function(TouchFunction_History),
									 new Function(TouchFunction_HistoryDel) };
		fCallSgn_tax 		= false;
		fDisplaySale	 	= true;
		fCallSgn_sale		= false;

	}

	public void		TouchEvent_Main(){

		if( Input.GetMouseButtonDown (0) ){
			
			ray = GameObject.Find ( "MainCamera" ).camera.ScreenPointToRay ( Input.mousePosition );
			
			if( Physics.Raycast ( ray.origin, ray.direction, out HitData, Mathf.Infinity ) ){

				TouchButtonName = HitData.collider.name;
				TouchEvent_SearchButton ();

			}
			
		}

	}

	public void		TouchEvent_SearchButton(){

		int 	LoopMax 	= GameObject.Find ("Panel").transform.childCount;
		Panel	PanelData 	= GameObject.Find ("Panel").GetComponent<Panel>();

		for( int i = 0 ; i < LoopMax; i++ ){
			if( TouchButtonName == PanelData.GetName (i) ){

				MethodNum 	= i-2;
				fInput 		= true;

				GameObject.Find ("History").GetComponent<History>().HistorySetDefault ();

				return;

			} else if( TouchButtonName == "HistoryNode" ){
		
				MethodNum 	= 19;
				fInput 		= true;
				return;

			} else if( TouchButtonName == "HistoryDelNode" ){

				MethodNum 	= 20;
				fInput 		= true;
				return;

			}
		}

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	TouchFunction
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Number
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_Number(){

		if( fCallSgn_tax == false ){
			if( fCallSgn_sale == true ){

				Sgn_sale	Obj 	= GameObject.Find ("Sgn_sale").GetComponent<Sgn_sale>();
				
				Obj.InputValueText 	= TouchButtonName;
				Obj.Sgn_sale_Process ();

				TouchButtonName 	= "";
				fInput 				= false;

			} else {

				Wnd_main	Obj 	= GameObject.Find ("Wnd_main").GetComponent<Wnd_main>();

				Obj.InputValueText 	= TouchButtonName;
				Obj.Wnd_main_Process ();

				TouchButtonName 	= "";
				fInput 				= false;

			}
		} else {

			Sgn_tax		Obj 	= GameObject.Find ("Sgn_tax").GetComponent<Sgn_tax>();

			Obj.InputValueText 	= TouchButtonName;
			Obj.Sgn_tax_Process ();

			TouchButtonName 	= "";
			fInput 				= false;

		}

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Delete
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_Delete(){

		if (fCallSgn_tax == true){

			TouchFunction_Sgn_Tax_Delete ();

		} else {
			if( fCallSgn_sale == true ){

				TouchFunction_Sgn_sale_Delete ();

			} else {

				TouchFunction_Wnd_main_Delete();

			}

		}
	

	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Sgn_sale_Delete
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_Sgn_sale_Delete(){

		Sgn_sale	Sgn_saleObj 	= GameObject.Find ("Sgn_sale").GetComponent<Sgn_sale> ();
		Wnd_main	Wnd_mainData 	= GameObject.Find ("Panel").transform.FindChild ("Wnd_main").GetComponent<Wnd_main>();
		Wnd_tax		Wnd_taxData 	= GameObject.Find ("Panel").transform.FindChild ("Wnd_tax").GetComponent<Wnd_tax>();
		
		if ( Sgn_saleObj.ValueText == "0")	return;
		
		if( Sgn_saleObj.nRank == 1 ){
			
			Sgn_saleObj.Sgn_sale_Reset();
			Sgn_saleObj.SetDiscountData();

			Wnd_mainData.nValueData = Wnd_mainData.nValueDataBuf;
			Wnd_mainData.ValueText 	= Wnd_mainData.nValueDataBuf.ToString ();
			Wnd_mainData.nRank 		= Wnd_mainData.ValueText.Length;

			Wnd_mainData.SetValueData ();
			Wnd_mainData.SetSpriteName ();
			Wnd_mainData.SetActiveNumber ();

			TouchButtonName		= "";
			fInput 				= false;
			nDeleteButtonCount 	= 0;
			
			return;
			
		}
		
		if (Input.GetMouseButton (0)) {
			
			nDeleteButtonCount++; 
			
		} else {
			if( nDeleteButtonCount < 60 ){
				
				Sgn_saleObj.transform.GetChild (Sgn_saleObj.nRank-1).gameObject.SetActive (false);
				
				for( int i = 0 ; i < Sgn_saleObj.nRank-1 ; i++ ){
					
					Sgn_saleObj.transform.GetChild (i).GetComponent<NumberNode>().GetComponent<UISprite>().spriteName = Sgn_saleObj.transform.GetChild (i+1).GetComponent<NumberNode>().GetComponent<UISprite>().spriteName;
					
				}
				
				Sgn_saleObj.nRank--;
				
				Sgn_saleObj.transform.GetChild ( Sgn_saleObj.nRank ).GetComponent<NumberNode>().GetComponent<UISprite>().spriteName = "num_s_00";
				Sgn_saleObj.ValueText = Sgn_saleObj.ValueText.Remove ( Sgn_saleObj.nRank, 1 );
				Sgn_saleObj.SetDiscountData();

				Wnd_mainData.SetValueData ();
				Wnd_mainData.SetSpriteName ();
				Wnd_mainData.SetActiveNumber ();

				Wnd_taxData.SetTaxValue ();
				Wnd_taxData.SetValueText ();
				Wnd_taxData.SetRank ();
				Wnd_taxData.SetActiveNumber ();
				Wnd_taxData.SetSpriteName ();
				Wnd_taxData.ValueOverProcess ();
				
				TouchButtonName		= "";
				fInput 				= false;
				nDeleteButtonCount 	= 0;
				
			} 
			
		}
		
		if( nDeleteButtonCount >= 60 ){
			
			///////////////////////////////////////////////////////
			//	Viblate
			///////////////////////////////////////////////////////
			Handheld.Vibrate();
			
			Sgn_saleObj.Sgn_sale_Reset ();

			Wnd_mainData.nValueData = Wnd_mainData.nValueDataBuf;
			Wnd_mainData.ValueText 	= Wnd_mainData.nValueDataBuf.ToString ();
			Wnd_mainData.nRank 		= Wnd_mainData.ValueText.Length;

			Wnd_mainData.SetValueData ();
			Wnd_mainData.SetSpriteName ();
			Wnd_mainData.SetActiveNumber ();
			
			TouchButtonName		= "";
			fInput 				= false;
			nDeleteButtonCount 	= 0;
			
		}

	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Wnd_main_Delete
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_Wnd_main_Delete(){

		Wnd_main	Wnd_mainObj = GameObject.Find ("Wnd_main").GetComponent<Wnd_main> ();
		
		if (Wnd_mainObj.ValueText == "0")	return;
		
		if (Wnd_mainObj.nRank == 1) {
			
			Wnd_mainObj.Wnd_main_Reset ();
			Wnd_mainObj.SetValueData ();
			
			GameObject.Find ("Wnd_tax").GetComponent<Wnd_tax>().Wnd_tax_Reset ();
			
			TouchButtonName		= "";
			fInput 				= false;
			nDeleteButtonCount 	= 0;
			
			return;
			
		}
		
		if (Input.GetMouseButton (0)) {
			
			nDeleteButtonCount++; 
			
		} else {
			
			if( nDeleteButtonCount < 60 ){
				
				Wnd_mainObj.transform.GetChild (Wnd_mainObj.nRank-1).gameObject.SetActive ( false );
				
				for( int i = 0 ; i < Wnd_mainObj.nRank-1; i++ ){
					
					Wnd_mainObj.transform.GetChild (i).GetComponent<NumberNode>().GetComponent<UISprite>().spriteName = Wnd_mainObj.transform.GetChild (i+1).GetComponent<NumberNode>().GetComponent<UISprite>().spriteName;
					Wnd_mainObj.transform.GetChild (i).GetComponent<NumberNode>().nNumber = Wnd_mainObj.transform.GetChild (i+1).GetComponent<NumberNode>().nNumber;

				}
				
				Wnd_mainObj.nRank--;
				
				Wnd_mainObj.transform.GetChild ( Wnd_mainObj.nRank ).GetComponent<NumberNode>().GetComponent<UISprite>().spriteName = "num_l_00";
				Wnd_mainObj.ValueText = Wnd_mainObj.ValueText.Remove ( Wnd_mainObj.nRank, 1 );
				
				TouchButtonName 	= "";
				fInput 				= false;
				nDeleteButtonCount 	= 0;
				
			}
			
		}
		
		if( nDeleteButtonCount >= 60 ){
			
			///////////////////////////////////////////////////////
			//	Viblate
			///////////////////////////////////////////////////////
			Handheld.Vibrate();
			
			Wnd_mainObj.Wnd_main_Reset ();
			GameObject.Find ("Wnd_tax").GetComponent<Wnd_tax>().Wnd_tax_Reset ();
			
			TouchButtonName	 	= "";
			fInput 				= false;
			nDeleteButtonCount 	= 0;
			
		}
		
		Wnd_mainObj.SetValueData ();
		
		if( Wnd_mainObj.nValueData == 0 )	GameObject.Find ("Wnd_tax").GetComponent<Wnd_tax>().Wnd_tax_Reset ();


	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Tax_Delete
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_Sgn_Tax_Delete(){

		Sgn_tax	Sgn_taxObj = GameObject.Find ("Sgn_tax").GetComponent<Sgn_tax> ();

		if ( Sgn_taxObj.ValueText == "0")	return;

		if( Sgn_taxObj.nRank == 1 ){

			Sgn_taxObj.Sgn_tax_Reset();
			Sgn_taxObj.SetValueData();

			TouchButtonName		= "";
			fInput 				= false;
			nDeleteButtonCount 	= 0;
			
			return;

		}

		if (Input.GetMouseButton (0)) {
			
			nDeleteButtonCount++; 

		} else {
			if( nDeleteButtonCount < 60 ){

				Sgn_taxObj.transform.GetChild (Sgn_taxObj.nRank-1).gameObject.SetActive (false);

				for( int i = 0 ; i < Sgn_taxObj.nRank-1 ; i++ ){

					Sgn_taxObj.transform.GetChild (i).GetComponent<NumberNode>().GetComponent<UISprite>().spriteName = Sgn_taxObj.transform.GetChild (i+1).GetComponent<NumberNode>().GetComponent<UISprite>().spriteName;

				}

				Sgn_taxObj.nRank--;

				Sgn_taxObj.transform.GetChild ( Sgn_taxObj.nRank ).GetComponent<NumberNode>().GetComponent<UISprite>().spriteName = "num_s_00";
				Sgn_taxObj.ValueText = Sgn_taxObj.ValueText.Remove ( Sgn_taxObj.nRank, 1 );

				TouchButtonName		= "";
				fInput 				= false;
				nDeleteButtonCount 	= 0;

			} 
		
		}

		if( nDeleteButtonCount >= 60 ){

			///////////////////////////////////////////////////////
			//	Viblate
			///////////////////////////////////////////////////////
			Handheld.Vibrate();

			Sgn_taxObj.Sgn_tax_Reset ();

			TouchButtonName		= "";
			fInput 				= false;
			nDeleteButtonCount 	= 0;

		}

	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Tax
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_Tax(){

		if (fCallSgn_sale == true)	return;

		if( fCallSgn_tax == false ){

			GameObject.Find ("Panel").transform.FindChild ("Bgmask").gameObject.SetActive( true );

			GameObject.Find ("Panel").transform.FindChild ("TotalValue").gameObject.SetActive ( false );
			GameObject.Find ("Panel").transform.FindChild ("TotalTaxValue").gameObject.SetActive ( false );
			GameObject.Find ("Panel").transform.FindChild ("Parenthesis").gameObject.SetActive ( false );

			fCallSgn_tax 		= true;
			fDisplaySale 		= false;
			TouchButtonName 	= "";
			fInput 				= false;

		}

	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	OK
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_OK(){

		if( fCallSgn_sale == false ){
	
			GameObject.Find ("Sgn_tax").GetComponent<Sgn_tax> ().SetValueData ();

			int	nLoopMax = GameObject.Find ("History").GetComponent<History>().nAddHistoryCount;

			for( int i = 0 ; i < nLoopMax ; i++ ){

				HistoryNode		child = GameObject.Find ("History").transform.GetChild (i).GetComponent<HistoryNode>();
				child.ValueUpdate ();
			
			}

			TotalValue	TotalObj = GameObject.Find ("TotalValue").GetComponent<TotalValue>();
			
			TotalObj.TotalValue_Update();
			TotalObj.SetActiveNumber();
			TotalObj.SetSpriteName();
			
			TotalTaxValue	TotalTaxObj = GameObject.Find ("TotalTaxValue").GetComponent<TotalTaxValue>();
			
			TotalTaxObj.TotalTaxValue_Update();
			TotalTaxObj.SetActiveNumber();
			TotalTaxObj.SetSpriteName();

			fCallSgn_tax 	= false;

		} 

		Transform	BgmaskData = GameObject.Find ("Panel").transform.FindChild ("Bgmask");
		
		BgmaskData.gameObject.SetActive( false );
		BgmaskData.GetComponent<UISprite> ().depth = 17;

		fDisplaySale 		= true;
		TouchButtonName 	= "";
		fInput 				= false;

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	AddHistory
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_AddHistory(){
	
		if (GameObject.Find ("Wnd_tax").GetComponent<Wnd_tax> ().nTaxValue == 0)						return;
		if (GameObject.Find ("Wnd_tax").GetComponent<Wnd_tax> ().fValueOver == true)					return;
		if (GameObject.Find ("TotalTaxValue").GetComponent<TotalTaxValue> ().nTotalTaxValue >= 9999999)	return;	

		History HistoryObj = GameObject.Find ("History").GetComponent<History> ();
		
		if (HistoryObj.nAddHistoryCount >= HistoryObj.nHistoryCountMax)	return;
		
		HistoryObj.nAddHistoryCount++;
		
		HistoryObj.SetActiveHistory ();
		HistoryObj.HistoryUpdate ();
		HistoryObj.SetHistoryData ();

		TotalValue		TotalObj = GameObject.Find("TotalValue").transform.GetComponent<TotalValue>();
		
		TotalObj.TotalValue_Update ();
		TotalObj.SetActiveNumber ();
		TotalObj.SetSpriteName();
		
		TotalTaxValue	TotalTaxObj = GameObject.Find ("TotalTaxValue").transform.GetComponent<TotalTaxValue>();
		
		TotalTaxObj.TotalTaxValue_Update ();
		TotalTaxObj.SetActiveNumber ();
		TotalTaxObj.SetSpriteName ();
		
		GameObject.Find ("Wnd_tax").GetComponent<Wnd_tax> ().Wnd_tax_Reset ();
		GameObject.Find ("Wnd_main").GetComponent<Wnd_main> ().Wnd_main_Reset ();

		if( fCallSgn_sale == true ){

			GameObject.Find ("Wnd_tax").GetComponent<Wnd_tax> ().Wnd_tax_Reset ();
			GameObject.Find ("Wnd_main").GetComponent<Wnd_main> ().Wnd_main_Reset ();

			Transform	BgmaskData 		= GameObject.Find ("Panel").transform.FindChild ("Bgmask");
			Sgn_sale	Sgn_saleData	= GameObject.Find ("Panel").transform.FindChild("Sgn_sale").GetComponent<Sgn_sale>();

			BgmaskData.gameObject.SetActive( false );
			BgmaskData.GetComponent<UISprite> ().depth = 17;

			Sgn_saleData.gameObject.SetActive ( false );
			Sgn_saleData.Sgn_sale_Reset ();

			GameObject.Find ("Panel").transform.FindChild ("Btn_del").gameObject.SetActive ( true );

			fDisplaySale 	= true;
			fCallSgn_sale 	= false;

		}
		
		TouchButtonName = "";
		fInput 			= false;

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Btn_10
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_Btn_10(){
		
		if (fDisplaySale == false){

			TouchFunction_Delete();
				
		} else {

			TouchFunction_Discount ();
			
		}
		
	}


	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Btn_11
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_Btn_11(){

		GameObject.Find ("Panel").transform.FindChild ("TotalValue").gameObject.SetActive ( true );
		GameObject.Find ("Panel").transform.FindChild ("TotalTaxValue").gameObject.SetActive ( true );
		GameObject.Find ("Panel").transform.FindChild ("Parenthesis").gameObject.SetActive ( true );

		if (fCallSgn_tax == true){

			TouchFunction_OK ();

		} else {

			TouchFunction_AddHistory();
		
		}

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Reset
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_Reset(){

		if (fCallSgn_tax == true)	return;
		if (fCallSgn_sale == true)	return;

		if( GameObject.Find ("History").GetComponent<History>().nAddHistoryCount == 0 &&
		    GameObject.Find ("TotalTaxValue").GetComponent<TotalTaxValue>().nTotalTaxValue == 0 &&
		    GameObject.Find ("Wnd_main").GetComponent<Wnd_main>().nValueData == 0 &&
		    GameObject.Find ("Wnd_tax").GetComponent<Wnd_tax>().nTaxValue == 0 ){

			return;

		} else {

			///////////////////////////////////////////////////////
			//	Viblate
			///////////////////////////////////////////////////////
			Handheld.Vibrate();

		}

		//	History
		if( GameObject.Find ("History").GetComponent<History>().nAddHistoryCount > 0 ){

			GameObject.Find ("History").GetComponent <History>().History_Reset ();

		}

		//	TotalValue
		if( GameObject.Find ("TotalValue").GetComponent<TotalValue>().nTotalValue != 0 ){
			
			GameObject.Find ("TotalValue").GetComponent<TotalValue> ().TotalValue_Reset ();
			GameObject.Find ("TotalValue").GetComponent<TotalValue> ().SetSpriteName ();
			
		}

		//	TotalTaxValue
		if( GameObject.Find ("TotalTaxValue").GetComponent<TotalTaxValue>().nTotalTaxValue != 0 ){

			GameObject.Find ("TotalTaxValue").GetComponent<TotalTaxValue> ().TotalTaxValue_Reset ();
			GameObject.Find ("TotalTaxValue").GetComponent<TotalTaxValue> ().SetSpriteName ();

		}
	
		//	Wnd_main
		if( GameObject.Find ("Wnd_main").GetComponent<Wnd_main>().nValueData != 0 ){

			GameObject.Find ("Wnd_main").GetComponent<Wnd_main> ().Wnd_main_Reset ();

		}

		//	Wnd_tax
		if( GameObject.Find ("Wnd_tax").GetComponent<Wnd_tax>().nTaxValue != 0 ){

			GameObject.Find ("Wnd_tax").GetComponent<Wnd_tax> ().Wnd_tax_Reset ();

		}
	
		TouchButtonName = "";
		fInput 			= false;

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	ChangeLayout
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_Layout(){

		if (fCallSgn_tax == true)	return;
		if (fCallSgn_sale == true)	return;

		Panel 			PanelData 		= GameObject.Find ("Panel").GetComponent<Panel> ();
		TotalValue		TotalData 		= GameObject.Find ("TotalValue").GetComponent<TotalValue>();
		TotalTaxValue 	TotalTaxData 	= GameObject.Find ("TotalTaxValue").GetComponent<TotalTaxValue> ();
		Parenthesis		ParenthesisData = GameObject.Find("Parenthesis").GetComponent<Parenthesis>();

		if( TouchButtonName == "Btn_function1" ){

			PanelData.nlayoutPtnNum = (int)LAYOUT_PTN.PTN0;

		} else if( TouchButtonName == "Btn_function2" ){

			PanelData.nlayoutPtnNum = (int)LAYOUT_PTN.PTN1;

		} else if( TouchButtonName == "Btn_function3" ){

			PanelData.nlayoutPtnNum = (int)LAYOUT_PTN.PTN2;

		} 

		PanelData.SaveLayoutPtn ();
		TotalData.SetNumberColor ();
		TotalTaxData.SetNumberColor ();
		ParenthesisData.SetParenthesisColor ();

		TouchButtonName = "";
		fInput 			= false;

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	History
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_History(){
	
		if (fCallSgn_tax == true)	return;
		if (fCallSgn_sale == true)	return;

		History 	HistoryData 	= GameObject.Find ("History").GetComponent<History> ();
		HistoryNode HistoryObj 		= HitData.collider.GetComponent<HistoryNode> ();
		GameObject 	HistoryDelObj 	= HistoryObj.transform.FindChild ("HistoryDelNode").gameObject;

		if (HistoryObj.fTouch == false){
			for( int i = 0 ; i < HistoryData.nAddHistoryCount ; i++ ){

				HistoryNode		Obj = GameObject.Find ("History").transform.GetChild (i).GetComponent<HistoryNode>();

				if( Obj.fTouch == true ){

					Obj.fTouch = false;
					Obj.transform.FindChild("HistoryDelNode").gameObject.SetActive ( false );

					return;

				}

			}

			HistoryObj.fTouch = true;
			HistoryDelObj.SetActive ( true );

		} else {

			HistoryObj.fTouch = false;
			HistoryDelObj.SetActive ( false );

		}

		TouchButtonName = "";
		fInput 			= false;

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	HistoryDel
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void		TouchFunction_HistoryDel(){

		if (fCallSgn_tax == true)	return;
		if (fCallSgn_sale == true)	return;

		History		HistoryObj		= HitData.collider.transform.parent.GetComponent<History>();
		HistoryNode HistoryNodeObj 	= HitData.collider.transform.parent.GetComponent<HistoryNode> ();	
		GameObject 	HistoryDelObj 	= HitData.collider.GetComponent<HistoryDelNode>().gameObject;

		HistoryNodeObj.fTouch = false;
		HistoryDelObj.SetActive ( false );

		HistoryObj = GameObject.Find ("History").GetComponent<History> ();
		HistoryObj.HistoryDelete ( HistoryNodeObj.nID );

		TouchButtonName = "";
		fInput 			= false;

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Reverse
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	public void	TouchFunction_Reverse(){

		if (fCallSgn_tax == true)	return;
		if (fCallSgn_sale == true)	return;

		Panel 	PanelData = GameObject.Find ("Panel").GetComponent<Panel> ();

		GameObject.Find ("Panel").GetComponent<Panel> ().nReversePtnNum ^= 0x01;

		GameObject.Find ("Wnd_main").GetComponent<Wnd_main> ().NumberPositionUpdate ();
	
		History HistoryData = GameObject.Find ("History").GetComponent<History> ();
		HistoryData.Reverse ();

		PanelData.SaveReversePtn ();
		GameObject.Find ("TotalTaxValue").GetComponent<TotalTaxValue> ().Reverse ();
		GameObject.Find ("TotalValue").GetComponent<TotalValue> ().Reverse ();
		GameObject.Find ("Parenthesis").GetComponent<Parenthesis> ().Reverse ();

		TouchButtonName = "";
		fInput 			= false;

	}

	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////
	//	Discount
	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////	
	public void	TouchFunction_Discount(){

		if (GameObject.Find ("Panel").transform.FindChild ("Wnd_main").GetComponent<Wnd_main> ().nValueData <= 0)						return;
		if (GameObject.Find ("Panel").transform.FindChild ("TotalTaxValue").GetComponent<TotalTaxValue> ().nTotalTaxValue >= 9999999)	return;

		Transform	BgmaskData 		= GameObject.Find ("Panel").transform.FindChild ("Bgmask");
		GameObject 	Sgn_saleData 	= GameObject.Find ("Panel").transform.FindChild ("Sgn_sale").gameObject;
		Wnd_main	Wnd_mainData 	= GameObject.Find ("Panel").transform.FindChild ("Wnd_main").GetComponent<Wnd_main>();

		Sgn_saleData.SetActive (true);

		BgmaskData.gameObject.SetActive( true );
		BgmaskData.GetComponent<UISprite> ().depth = 20;

		Wnd_mainData.nValueDataBuf = Wnd_mainData.nValueData;

		GameObject.Find ("Panel").transform.FindChild ("Btn_del").gameObject.SetActive ( false );

		GameObject.Find ("Panel").transform.FindChild ("TotalValue").gameObject.SetActive ( false );
		GameObject.Find ("Panel").transform.FindChild ("TotalTaxValue").gameObject.SetActive ( false );
		GameObject.Find ("Panel").transform.FindChild ("Parenthesis").gameObject.SetActive ( false );

		fDisplaySale 		= false;
		fCallSgn_sale 		= true;
		TouchButtonName 	= "";
		fInput 				= false;

	}

}
