﻿using UnityEngine;
using System.Collections;

public class ParenthesisNode : MonoBehaviour {

	public UISprite		spriteData;
	public Vector3		positionData;
	public Vector3		scaleData;
	
	public void			SetPositionX( float fX ){ positionData.x = fX; }
	public void			SetPositionY( float fY ){ positionData.y = fY; }
	public void			SetPositionZ( float fZ ){ positionData.z = fZ; }
	public void			SetScaleX( float fX ){ scaleData.x = fX; }
	public void			SetScaleY( float fY ){ scaleData.y = fY; }
	public void			SetScaleZ( float fZ ){ scaleData.z = fZ; }
	public void			SetSpriteName( string Name ){ spriteData.spriteName = Name; }
	public void			SetSpriteDimensions( int w, int h ){ spriteData.SetDimensions (w, h); }
	public void			SetSpriteColor ( Color color ){ spriteData.color = color; }
	public void			SetSpriteDepth ( int nDepth ){ spriteData.depth = nDepth; }
	public void			SetActive( bool flag ){ gameObject.SetActive (flag); }


	// Use this for initialization
	void Start () {
	
		Parenthesis_Init ();

	}

	public void		Parenthesis_Init(){

		transform.localPosition = positionData;
		transform.localScale 	= scaleData;

	}

}