﻿using UnityEngine;
using System.Collections;

public class Parenthesis : MonoBehaviour {

	public float[][]	fPositionX;
	public string[]		SpriteName;
	public GameObject	ParenthesisPrefab;

	// Use this for initialization
	void Start () {
	
		Parenthesis_Init ();

	}

	public void		Parenthesis_Init(){

		fPositionX = new float[][]{ 

			new[]{-130.0f, -4.0f },
			new[]{ -0.0f, 126.0f }

		};
		SpriteName = new string[]{ "num_s_11", "num_s_12" };
		CreateParenthesis ();

	}

	public void		CreateParenthesis(){

		int nReversePtnNum = PlayerPrefs.GetInt ("ReversePtnNum");

		for( int i = 0 ; i < 2 ; i++ ){

			CreateParenthesis ( SpriteName[i], fPositionX[nReversePtnNum][i] );

		}

	}

	public void		CreateParenthesis ( string spriteName, float fPositionX ){
		
		GameObject	Obj 		= Instantiate (ParenthesisPrefab) as GameObject;
		Obj.transform.parent 	= GameObject.Find ("Parenthesis").transform;
		
		ParenthesisNode NumberObj = Obj.GetComponent<ParenthesisNode> ();
		NumberObj.SetPositionX ( fPositionX );
		NumberObj.SetPositionY ( -362.0f );
		NumberObj.SetPositionZ ( 0.0f );
		NumberObj.SetScaleX ( 1.0f );
		NumberObj.SetScaleY ( 1.0f );
		NumberObj.SetScaleZ ( 1.0f );
		NumberObj.SetSpriteName ( spriteName );
		NumberObj.SetSpriteDimensions ( 20, 28 );
		NumberObj.SetSpriteColor ( Color.black );
		NumberObj.SetSpriteDepth (3);
		
	}

	public void		SetParenthesisColor(){

		Color[]		ColorData 	= new Color[]{ Color.black, Color.white, Color.black };
		int 		PtnNum 		= GameObject.Find ("Panel").GetComponent<Panel> ().nlayoutPtnNum;
		int 		nLoopMax 	= transform.childCount;

		for( int i = 0 ; i < nLoopMax ; i++ ){
			
			ParenthesisNode	Obj = transform.GetChild (i).GetComponent<ParenthesisNode>();
			
			Obj.SetSpriteColor ( ColorData[PtnNum] );
			
		}


	}

	public void		Reverse(){

		int nReversePtnNum = GameObject.Find ("Panel").GetComponent<Panel> ().nReversePtnNum;

		for( int i = 0 ; i < 2 ; i++ ){

			Vector3		UpdatePos = new Vector3( fPositionX[nReversePtnNum][i], -362.0f, 0.0f );
			transform.GetChild(i).GetComponent<ParenthesisNode>().positionData = UpdatePos;
			transform.GetChild (i).transform.localPosition = UpdatePos;

		}

	}

}
