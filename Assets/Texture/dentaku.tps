<?xml version="1.0" encoding="UTF-8"?>
<data version="1.0">
    <struct type="Settings">
        <key>fileFormatVersion</key>
        <int>3</int>
        <key>texturePackerVersion</key>
        <string>3.2.1</string>
        <key>autoSDSettings</key>
        <array>
            <struct type="AutoSDSettings">
                <key>scale</key>
                <double>1</double>
                <key>extension</key>
                <string></string>
                <key>spriteFilter</key>
                <string></string>
                <key>acceptFractionalValues</key>
                <false/>
                <key>maxTextureSize</key>
                <QSize>
                    <key>width</key>
                    <int>-1</int>
                    <key>height</key>
                    <int>-1</int>
                </QSize>
            </struct>
        </array>
        <key>allowRotation</key>
        <false/>
        <key>premultiplyAlpha</key>
        <false/>
        <key>shapeDebug</key>
        <false/>
        <key>dpi</key>
        <uint>72</uint>
        <key>dataFormat</key>
        <string>unity</string>
        <key>textureFileName</key>
        <filename>dentaku.png</filename>
        <key>flipPVR</key>
        <false/>
        <key>ditherType</key>
        <enum type="SettingsBase::DitherType">NearestNeighbour</enum>
        <key>backgroundColor</key>
        <uint>0</uint>
        <key>libGdx</key>
        <struct type="LibGDX">
            <key>filtering</key>
            <struct type="LibGDXFiltering">
                <key>x</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
                <key>y</key>
                <enum type="LibGDXFiltering::Filtering">Linear</enum>
            </struct>
        </struct>
        <key>shapePadding</key>
        <uint>2</uint>
        <key>jpgQuality</key>
        <uint>80</uint>
        <key>pngOptimizationLevel</key>
        <uint>0</uint>
        <key>textureSubPath</key>
        <string></string>
        <key>textureFormat</key>
        <enum type="SettingsBase::TextureFormat">png</enum>
        <key>borderPadding</key>
        <uint>2</uint>
        <key>maxTextureSize</key>
        <QSize>
            <key>width</key>
            <int>4096</int>
            <key>height</key>
            <int>2048</int>
        </QSize>
        <key>fixedTextureSize</key>
        <QSize>
            <key>width</key>
            <int>-1</int>
            <key>height</key>
            <int>-1</int>
        </QSize>
        <key>reduceBorderArtifacts</key>
        <false/>
        <key>algorithmSettings</key>
        <struct type="AlgorithmSettings">
            <key>algorithm</key>
            <enum type="AlgorithmSettings::AlgorithmId">MaxRects</enum>
            <key>freeSizeMode</key>
            <enum type="AlgorithmSettings::AlgorithmFreeSizeMode">Best</enum>
            <key>sizeConstraints</key>
            <enum type="AlgorithmSettings::SizeConstraints">POT</enum>
            <key>forceSquared</key>
            <false/>
            <key>forceWordAligned</key>
            <false/>
            <key>maxRects</key>
            <struct type="AlgorithmMaxRectsSettings">
                <key>heuristic</key>
                <enum type="AlgorithmMaxRectsSettings::Heuristic">Best</enum>
            </struct>
            <key>basic</key>
            <struct type="AlgorithmBasicSettings">
                <key>sortBy</key>
                <enum type="AlgorithmBasicSettings::SortBy">Best</enum>
                <key>order</key>
                <enum type="AlgorithmBasicSettings::Order">Ascending</enum>
            </struct>
        </struct>
        <key>andEngine</key>
        <struct type="AndEngine">
            <key>minFilter</key>
            <enum type="AndEngine::MinFilter">Linear</enum>
            <key>packageName</key>
            <string>Texture</string>
            <key>wrap</key>
            <struct type="AndEngineWrap">
                <key>s</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
                <key>t</key>
                <enum type="AndEngineWrap::Wrap">Clamp</enum>
            </struct>
            <key>magFilter</key>
            <enum type="AndEngine::MagFilter">MagLinear</enum>
        </struct>
        <key>dataFileNames</key>
        <map type="GFileNameMap">
            <key>data</key>
            <struct type="DataFile">
                <key>name</key>
                <filename>dentaku.txt</filename>
            </struct>
        </map>
        <key>multiPack</key>
        <false/>
        <key>forceIdenticalLayout</key>
        <false/>
        <key>outputFormat</key>
        <enum type="SettingsBase::OutputFormat">RGBA8888</enum>
        <key>contentProtection</key>
        <struct type="ContentProtection">
            <key>key</key>
            <string></string>
        </struct>
        <key>autoAliasEnabled</key>
        <true/>
        <key>trimSpriteNames</key>
        <false/>
        <key>cleanTransparentPixels</key>
        <true/>
        <key>globalSpriteSettings</key>
        <struct type="SpriteSettings">
            <key>scale</key>
            <double>1</double>
            <key>scaleMode</key>
            <enum type="ScaleMode">Smooth</enum>
            <key>innerPadding</key>
            <uint>0</uint>
            <key>extrude</key>
            <uint>0</uint>
            <key>trimThreshold</key>
            <uint>1</uint>
            <key>trimMode</key>
            <enum type="SpriteSettings::TrimMode">Trim</enum>
            <key>heuristicMask</key>
            <false/>
        </struct>
        <key>fileList</key>
        <array>
            <filename>../素材/bg1a.png</filename>
            <filename>../素材/bg1b.png</filename>
            <filename>../素材/bg2a.png</filename>
            <filename>../素材/bg2b.png</filename>
            <filename>../素材/bg3a.png</filename>
            <filename>../素材/bg3b.png</filename>
            <filename>../素材/bgmask1.png</filename>
            <filename>../素材/bgmask2.png</filename>
            <filename>../素材/bgmask3.png</filename>
            <filename>../素材/bgmask4.png</filename>
            <filename>../素材/btn_00.png</filename>
            <filename>../素材/btn_01.png</filename>
            <filename>../素材/btn_1del.png</filename>
            <filename>../素材/btn_02.png</filename>
            <filename>../素材/btn_03.png</filename>
            <filename>../素材/btn_04.png</filename>
            <filename>../素材/btn_05.png</filename>
            <filename>../素材/btn_06.png</filename>
            <filename>../素材/btn_07.png</filename>
            <filename>../素材/btn_08.png</filename>
            <filename>../素材/btn_09.png</filename>
            <filename>../素材/btn_10.png</filename>
            <filename>../素材/btn_11.png</filename>
            <filename>../素材/btn_12.png</filename>
            <filename>../素材/btn_13.png</filename>
            <filename>../素材/btn_d1.png</filename>
            <filename>../素材/btn_d2.png</filename>
            <filename>../素材/btn_d3.png</filename>
            <filename>../素材/btn_function1.png</filename>
            <filename>../素材/btn_function2.png</filename>
            <filename>../素材/btn_function3.png</filename>
            <filename>../素材/btn_function4.png</filename>
            <filename>../素材/btn_historydel.png</filename>
            <filename>../素材/btn_reset.png</filename>
            <filename>../素材/btn_tax.png</filename>
            <filename>../素材/btn2_00.png</filename>
            <filename>../素材/btn2_01.png</filename>
            <filename>../素材/btn2_1del.png</filename>
            <filename>../素材/btn2_02.png</filename>
            <filename>../素材/btn2_03.png</filename>
            <filename>../素材/btn2_04.png</filename>
            <filename>../素材/btn2_05.png</filename>
            <filename>../素材/btn2_06.png</filename>
            <filename>../素材/btn2_07.png</filename>
            <filename>../素材/btn2_08.png</filename>
            <filename>../素材/btn2_09.png</filename>
            <filename>../素材/btn2_10.png</filename>
            <filename>../素材/btn2_11.png</filename>
            <filename>../素材/btn2_12.png</filename>
            <filename>../素材/btn2_13.png</filename>
            <filename>../素材/btn2_function1.png</filename>
            <filename>../素材/btn2_function2.png</filename>
            <filename>../素材/btn2_function3.png</filename>
            <filename>../素材/btn2_function4.png</filename>
            <filename>../素材/btn2_historydel.png</filename>
            <filename>../素材/btn2_reset.png</filename>
            <filename>../素材/btn2_tax.png</filename>
            <filename>../素材/btn3_00.png</filename>
            <filename>../素材/btn3_01.png</filename>
            <filename>../素材/btn3_1del.png</filename>
            <filename>../素材/btn3_02.png</filename>
            <filename>../素材/btn3_03.png</filename>
            <filename>../素材/btn3_04.png</filename>
            <filename>../素材/btn3_05.png</filename>
            <filename>../素材/btn3_06.png</filename>
            <filename>../素材/btn3_07.png</filename>
            <filename>../素材/btn3_08.png</filename>
            <filename>../素材/btn3_09.png</filename>
            <filename>../素材/btn3_10.png</filename>
            <filename>../素材/btn3_11.png</filename>
            <filename>../素材/btn3_12.png</filename>
            <filename>../素材/btn3_13.png</filename>
            <filename>../素材/btn3_function1.png</filename>
            <filename>../素材/btn3_function2.png</filename>
            <filename>../素材/btn3_function3.png</filename>
            <filename>../素材/btn3_function4.png</filename>
            <filename>../素材/btn3_historydel.png</filename>
            <filename>../素材/btn3_reset.png</filename>
            <filename>../素材/btn3_tax.png</filename>
            <filename>../素材/num_l_00.png</filename>
            <filename>../素材/num_l_01.png</filename>
            <filename>../素材/num_l_02.png</filename>
            <filename>../素材/num_l_03.png</filename>
            <filename>../素材/num_l_04.png</filename>
            <filename>../素材/num_l_05.png</filename>
            <filename>../素材/num_l_06.png</filename>
            <filename>../素材/num_l_07.png</filename>
            <filename>../素材/num_l_08.png</filename>
            <filename>../素材/num_l_09.png</filename>
            <filename>../素材/num_l_10.png</filename>
            <filename>../素材/num_s_00.png</filename>
            <filename>../素材/num_s_01.png</filename>
            <filename>../素材/num_s_02.png</filename>
            <filename>../素材/num_s_03.png</filename>
            <filename>../素材/num_s_04.png</filename>
            <filename>../素材/num_s_05.png</filename>
            <filename>../素材/num_s_06.png</filename>
            <filename>../素材/num_s_07.png</filename>
            <filename>../素材/num_s_08.png</filename>
            <filename>../素材/num_s_09.png</filename>
            <filename>../素材/num_s_10.png</filename>
            <filename>../素材/num_s_11.png</filename>
            <filename>../素材/num_s_12.png</filename>
            <filename>../素材/sgn_history_l.png</filename>
            <filename>../素材/sgn_over.png</filename>
            <filename>../素材/sgn_sale.png</filename>
            <filename>../素材/sgn_tax.png</filename>
            <filename>../素材/sgn2_history_l.png</filename>
            <filename>../素材/sgn2_sale.png</filename>
            <filename>../素材/sgn2_tax.png</filename>
            <filename>../素材/sgn3_history_l.png</filename>
            <filename>../素材/sgn3_sale.png</filename>
            <filename>../素材/sgn3_tax.png</filename>
            <filename>../素材/wnd_main.png</filename>
            <filename>../素材/wnd_tax.png</filename>
            <filename>../素材/wnd2_main.png</filename>
            <filename>../素材/wnd2_tax.png</filename>
            <filename>../素材/wnd3_main.png</filename>
            <filename>../素材/wnd3_tax.png</filename>
        </array>
        <key>ignoreFileList</key>
        <array/>
        <key>replaceList</key>
        <array/>
        <key>ignoredWarnings</key>
        <array/>
        <key>commonDivisorX</key>
        <uint>1</uint>
        <key>commonDivisorY</key>
        <uint>1</uint>
    </struct>
</data>
